<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js lt-ie10 lt-ie9 lt-ie8 lt-ie7" lang="en-us"> <![endif]-->
<!--[if IE 7]> <html class="no-js lt-ie10 lt-ie9 lt-ie8" lang="en-us"> <![endif]-->
<!--[if IE 8]> <html class="no-js lt-ie10 lt-ie9" lang="en-us"> <![endif]-->
<!--[if IE 9]> <html class="no-js lt-ie10 lt-ie9" lang="en-us"> <![endif]-->
<!--[if lt IE 10]> <html class="no-js lt-ie10" lang="en-us"> <![endif]-->
<!--[if !IE]> > <![endif]-->
<html class='no-js' lang='en'>
<!-- <![endif] -->
<head>
<meta name="description" content="" />
<meta name="author" content="" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />

<title>Stars 100 million Challenge - Predict the World Cup</title>

<meta content='initial-scale=1.0,user-scalable=no,maximum-scale=1,width=device-width' name='viewport' />
<meta content='yes' name='apple-mobile-web-app-capable'>
<meta content='translucent-black' name='apple-mobile-web-app-status-bar-style'>
<link href='images/favicon.png' rel='shortcut icon'>
<link href='images/favicon.ico' rel='icon' type='image/ico'>

<link href="https://fonts.googleapis.com/css?family=Poiret+One&amp;subset=cyrillic,latin-ext" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:300,300i,400,400i,700,700i" rel="stylesheet">


<link href="css/style.css" media="all" rel="stylesheet" type="text/css" />
<link href="css/bootstrap.min.css" media="all" rel="stylesheet" type="text/css" />
<script src="js/modernizr.js" type="text/javascript"></script>
    <!--[if IE]>
	<script src="js/html5.js"></script>
	<![endif]-->

</head>
<body>

<div id="wrapper">
    
    <div class="top-div">
        <header>
            <!-- header -->
            <!-- top header  -->
                <div class="top-header" id="page-top">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-8">
                                
                            </div>
                            <div class="col-md-4">
                                <ul class="list-inline">
                                    <li><a class="top-p">Responsible Gaming</a></li>
                                    <li><a class="top-p">24/7 Customer Service</a></li>
                                    <li><a class="top-p">English</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="clear"></div>
                    </div>
                </div><!-- end of top -->
                <!-- end of top header  -->

                <!-- navigation -->
            <!-- end of header -->
        </header>

        <div class="header-div">

            <div class="header-top-div clearfix">
                <div class="container">
                    <div class="row row-1">
                        <div class="col-md-4">
                            <div class="logo-div">
                                <a href="index.html"><img src="images/logo.png" alt="logo" class="img-responsive" /></a>
                            </div><!-- end of logo-div -->
                        </div>    
                        <div class="col-md-3">
                            <div class="middle-header">
                                <img src="images/spade-grey.png" alt=""><a href="#" class="a-header">POKER</a>
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="col-md-4 nopadding-right-10">
                                <div class="block-div">
                                    <form>
                                        <div class="form-group">
                                            <input type="text" class="form-control input-1" placeholder="Stars ID" name="starid">
                                        </div>
                                        <div class="form-group">
                                            <input type="password" class="form-control input-1" id="pwd" placeholder="Password" name="pwd">
                                        </div>
                                    </form>    
                                </div>
                            </div>    
                            <div class="col-md-4 nopadding">
                                <div class="middle-btn">
                                    <a class="btn btn-custom-login" href="#">Login</a>
                                    <a class="btn-forgot" href="#">Forgot Password</a>
                                </div>
                            </div>
                            <div class="col-md-4 nopadding">
                                <div class="middle-btn">
                                    <a class="btn btn-custom-join" href="#">Join now</a>
                                </div>
                            </div>
                        </div>    
                    </div>
                </div>
            </div><!-- end of header-top-div -->

        </div>
    </div><!-- end of top -->

	<div class="banner-container">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="col-md-4"></div>
                    <div class="col-md-4">
                        <div class="banner-caption-div-1 text-center">
                            <img src="images/header-star.png" class="img-banner" alt="star">
                        </div>        
                    </div>    
                    <div class="col-md-4">
                        <div class="start-time-container">
                            <p>Entries Available Until:</p> 
                            <p>
                                <span class="start-time-number">39</span>
                                <span class="start-time-label">Days</span>
                                <span class="start-time-number">15</span>
                                <span class="start-time-label">Hours</span>
                                <span class="start-time-number">0</span>
                                <span class="start-time-label">Minutes</span>
                                <span class="start-time-number">34</span>
                                <span class="start-time-label">Seconds</span>
                            </p>
                            <p class="terms-link">
                                <a href="#" class="alink"><span>Terms &amp; Conditions Apply</span></a>
                            </p>
                        </div>
                    </div>
                </div>
               
            </div>
        </div>        
    </div><!-- end of banner-container -->
    
    
    <div class="content-area clearfix">
     
        <section class="group_stage_section" id="group_stage">
            <div class="group_stage_div">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12 wow fadeInUp mob-work-part animated animation-name-1">
                            <div class="top_group_stage">
                               <h4 class="h4-1">Group Stage</h4> 
                               <p class="p-1">Select the outcome of every Group Stage match</p>
                            </div>
                        </div>
                    </div><!-- end of row -->
                </div><!-- end of container -->            
            </div><!-- end of group_stage_div -->
        </section><!-- end of group_stage_section -->

        <section class="group_match_section" id="group_stage">
            <div class="group_match_div">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12 wow fadeInUp mob-work-part animated animation-name-1">
                            <div class="top_group_match">
                                <ul class="bracket-groups">
                                    <li class="bracket-group">
                                        <div class="row bracket-group-row">
                                            <div class="column matches-column">
                                                <div class="bracket-group-matches">
                                                    <div class="group-matchup">
                                                        <div class="row matchup-row">
                                                            <div class="column shrink">
                                                                <div data-translationtip="bracketText.drawLabel" class="matchup-draw draw-selected">Draw</div>
                                                            </div> 
                                                            <div class="column left-space-column"></div>
                                                            <div class="column matchup-team ">
                                                                <img src="https://dm63aeeijtc75.cloudfront.net/app/i18nImages/bracket-challenge/russia.png" class="matchup-flag"> 
                                                                <span class="matchup-team-name">Russia</span>
                                                            </div> 
                                                            <div class="column text-right matchup-team ">
                                                                <span class="matchup-team-name">Saudi Arabia</span> 
                                                                <img src="https://dm63aeeijtc75.cloudfront.net/app/i18nImages/bracket-challenge/saudi-arabia.png" class="matchup-flag">
                                                            </div>
                                                        </div> <!---->
                                                    </div>
                                                    <div class="group-matchup">
                                                        <div class="row matchup-row">
                                                            <div class="column shrink">
                                                                <div data-translationtip="bracketText.drawLabel" class="matchup-draw ">Draw</div>
                                                            </div> 
                                                            <div class="column left-space-column"></div>
                                                            <div class="column matchup-team ">
                                                                <img src="https://dm63aeeijtc75.cloudfront.net/app/i18nImages/bracket-challenge/egypt.png" class="matchup-flag">
                                                                <span class="matchup-team-name">Egypt</span>
                                                            </div> 
                                                            <div class="column text-right matchup-team ">
                                                                <span class="matchup-team-name">Uruguay</span>
                                                                <img src="https://dm63aeeijtc75.cloudfront.net/app/i18nImages/bracket-challenge/uruguay.png" class="matchup-flag"></div>
                                                            </div> 
                                                            <div class="row matchup-divider"><p data-translationtip="bracketText.matchdayLabel">Matchday 1</p></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>    
                                    </ul>   
                                </div>
                            </div>
                        </div><!-- end of row -->
                    </div><!-- end of container -->            
                </div><!-- end of group_match_div -->
            </section><!-- end of group_match_section -->
        
        
        
        
        
        
        
        
        <!---
        <section class="contactus-section" id="contactus">
            <div class="contactus-div">
                <div class="container">
                    <div class="row">
                        <div class="col-md-4 col-sm-4 col-xs-12 wow fadeInUp mob-work-part animated animation-name-1">
                            <div class="footer-bottom-bg p-10">
                                <a href="index.html"><img class="img-responsive footer-img" src="images/logo.png" alt="logo" class="img-center" /></a>
                                <p class="p-center">Lorem ipsum dolor sit amet consectetur adipisicing elit. Reiciendis sit obcaecati consectetur ex officiis similique nihil odit placeat ut iste esse consequatur earum eum rerum ea ipsa quae, saepe qui unde fugit corporis molestias voluptatibus. Tempore eos fuga excepturi quo natus vitae corporis harum sequi pariatur, quae, deleniti reprehenderit suscipit.</p>
                            </div>
                        </div>

                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <section class="single-contact_us">
                                <div class="left_contact">
                                    <ul class="list catagories">
                                        <li><a><i class="fa fa-home color1"></i>No 271,Lorem ipsum dolor sit amet consectetur , USA.</a></li>
                                        <li><a><i class="fa fa-envelope color1"></i><span>user@gmail.com</span></a></li>
                                        <li><a><i class="fa fa-phone color1"></i>+91 (321) 777 777 7777<br>1800-0000-000-0000</a></li>
                                    </ul>
                                </div>
                            </section>    
                        </div>
                       
                        <div class="col-md-4 col-sm-4 col-xs-12 wow fadeInUp mob-work-part animated animation-name-1">
                            <div class="contactus-bg">
                                <div class="form-div">
                                    <form>
                                        <div class="field-group">
                                            <input type="text" name="name" class="input-field" placeholder="Full Name">
                                        </div>
                                        <div class="field-group">
                                            <input type="text" name="email" class="input-field" placeholder="Email address">
                                        </div>
                                        <div class="field-group">
                                            <input type="text" name="phone" class="input-field" placeholder="Phone Number">
                                        </div>
                                        <div class="field-group">
                                            <textarea name="message" id="" cols="30" rows="5"  class="input-field"  placeholder="Message"></textarea>
                                        </div>
                                      
                                        <div class="field-group">
                                            <button class="contact-btn">Contact us</button>
                                        </div>
                                    </form>
                                </div>
                            </div>    
                        </div>
                    </div>
                </div>
            </div>
        </section><!-- end of our-work-part-section -->

        <footer>
            <div class="copyright_wrapper">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="copyright_text">
                                <p class="font_champagne">Copyright &copy; 2018 starschallenge.betstars.uk   </p>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="footer_social_icons">
                                <ul class="social_logos-footer list-inline">
                                    <li class="fb"><a href="#" alt="Facebook icon" title="Facebook" class="icon" target="_blank"><i class="fab fa-facebook-square"></i></a></li>
                                    <li class="tw"><a href="#" alt="Twitter icon" title="Twitter" class="icon" target="_blank"><i class="fab fa-twitter-square"></i></a></li>
                                    <li class="gp"><a href="#" alt="Google Plus icon" title="Google Plus" class="icon" target="_blank"><i class="fab fa-google-plus-square"></i></a></li>
                                    <li class="yt"><a href="#" alt="Google Plus icon" title="youtube Plus"  class="icon" target="_blank"><i class="fab fa-youtube-square"></i></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>

    </div><!-- end of content-area -->
    

</div><!-- end of wrapper -->


<script src="js/jquery.min.js" type="text/javascript"></script> 
<script src="js/bootstrap.min.js" type="text/javascript"></script>
<link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">

<script type="text/javascript" src="js/custom.js"></script> 

<script>
    $(".down-arrow").click(function(e) {
		e.preventDefault();
		var section = $(this).attr("href");
		$("html, body").animate({
			scrollTop: $(section).offset().top-0
		});
		
	});
</script>

</body>
</html>
